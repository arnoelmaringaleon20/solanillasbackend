package com.example.solanillas.service.transformer;

import com.example.solanillas.domain.Edificaciones;
import com.example.solanillas.service.dto.EdificacionesDTO;

public class EdificacionesTransformer {
    public  static EdificacionesDTO getEdificacionesDtoFromedificaciones(Edificaciones edificaciones){
        if (edificaciones == null){
            return  null;
        }
        EdificacionesDTO dto = new EdificacionesDTO();

        //set variables
        dto.setId(edificaciones.getId());
        dto.setNombreProyecto(edificaciones.getNombreProyecto());
        dto.setFechaInicio(edificaciones.getFechaInicio());
        dto.setFechaFin(edificaciones.getFechaFin());
        dto.setCiudad(edificaciones.getCiudad());
        dto.setCostoProyecto(edificaciones.getCostoProyecto());
        return dto;
    }

    public  static Edificaciones getedificacionesFromEdificacionesDTO (EdificacionesDTO dto) {
        if (dto == null) {
            return null;
        }

        Edificaciones edificaciones = new Edificaciones();
        edificaciones.setId(dto.getId());
        edificaciones.setNombreProyecto(dto.getNombreProyecto());
        edificaciones.setFechaInicio(dto.getFechaInicio());
        edificaciones.setFechaFin(dto.getFechaFin());
        edificaciones.setCiudad(dto.getCiudad());
        return edificaciones;
    }
}
