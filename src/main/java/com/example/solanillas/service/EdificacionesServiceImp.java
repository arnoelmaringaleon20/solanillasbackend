package com.example.solanillas.service;

import com.example.solanillas.domain.Edificaciones;
import com.example.solanillas.repository.EdificacionesRepository;
import com.example.solanillas.service.dto.EdificacionesDTO;
import com.example.solanillas.service.transformer.EdificacionesTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
@Service
public class EdificacionesServiceImp implements EdificacionesService{

    @Autowired
    EdificacionesRepository edificacionesRepository;

    @Override
    public ResponseEntity create(EdificacionesDTO edificacionesDTO) {
        Edificaciones edificaciones = EdificacionesTransformer.getedificacionesFromEdificacionesDTO(edificacionesDTO);
        EdificacionesTransformer.getEdificacionesDtoFromedificaciones(edificacionesRepository.save(edificaciones));
        return new ResponseEntity(edificacionesDTO, HttpStatus.OK);
    }

    @Override
    public Page<EdificacionesDTO> read(Integer pageSize, Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return edificacionesRepository.findAll(pageable)
                .map(EdificacionesTransformer::getEdificacionesDtoFromedificaciones);
    }

    @Override
    public EdificacionesDTO update(EdificacionesDTO edificacionesDTO) {
        Edificaciones edificaciones = EdificacionesTransformer.getedificacionesFromEdificacionesDTO(edificacionesDTO);
        EdificacionesTransformer.getEdificacionesDtoFromedificaciones(edificacionesRepository.save(edificaciones));
        return edificacionesDTO;
    }
}
