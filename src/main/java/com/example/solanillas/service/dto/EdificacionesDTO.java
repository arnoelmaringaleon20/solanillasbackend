package com.example.solanillas.service.dto;


import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class EdificacionesDTO {
    @NotNull
    private int id;
    private String nombreProyecto;
    private LocalDate fechaInicio;
    private LocalDate fechaFin;
    private String ciudad;
    private int costoProyecto;
}
