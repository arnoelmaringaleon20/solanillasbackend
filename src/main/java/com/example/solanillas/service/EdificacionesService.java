package com.example.solanillas.service;

import com.example.solanillas.service.dto.EdificacionesDTO;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

public interface EdificacionesService {
    public ResponseEntity create(EdificacionesDTO EdificacionesDTO);
    public Page<EdificacionesDTO> read(Integer pageSize, Integer pageNumber);
    public EdificacionesDTO update(EdificacionesDTO EdificacionesDTO);
}

