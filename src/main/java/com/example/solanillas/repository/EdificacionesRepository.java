package com.example.solanillas.repository;

import com.example.solanillas.domain.Edificaciones;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EdificacionesRepository extends JpaRepository<Edificaciones, Integer> {
}
