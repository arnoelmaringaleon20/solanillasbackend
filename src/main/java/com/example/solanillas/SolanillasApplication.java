package com.example.solanillas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SolanillasApplication {

	public static void main(String[] args) {
		SpringApplication.run(SolanillasApplication.class, args);
	}

}
