package com.example.solanillas.web.rest;

import com.example.solanillas.domain.Edificaciones;
import com.example.solanillas.service.EdificacionesService;
import com.example.solanillas.service.dto.EdificacionesDTO;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

@RestController
@RequestMapping("/api")
@Api(value = "web resource", description = "web service for CRUD resource")

public class EdificacionesResource {
    @Autowired
    EdificacionesService edificacionesService;

    @GetMapping("/edificaciones")
    public Page<EdificacionesDTO> read(
            @PathParam("pageSize") Integer pageSize,
            @PathParam("pageNumber") Integer pageNumber){
        return  edificacionesService.read(pageSize, pageNumber);
    }


    @PostMapping("/edificaciones")
    public ResponseEntity create(@RequestBody EdificacionesDTO edificacionesDTO) {
        return edificacionesService.create(edificacionesDTO);
    }


    @PutMapping("/edificaciones")
    public EdificacionesDTO update(@RequestBody EdificacionesDTO edificacionesDTO) {

        return edificacionesService.update(edificacionesDTO);
    }
}
