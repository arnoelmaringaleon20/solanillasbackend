package com.example.solanillas.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor


public class Edificaciones {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private int id;
    private String nombreProyecto;
    private LocalDate fechaInicio;
    private LocalDate fechaFin;
    private String ciudad;
    private int costoProyecto;
}
